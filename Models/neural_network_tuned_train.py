"""Neural Network Tuned Train

This script allows the user to train a multi-layer perceptron with the
optimal hyperparameters found through a randomized search on the Student
Alcohol Consumption data set found on Kaggle:
https://www.kaggle.com/uciml/student-alcohol-consumption.

The trained model is stored in the /ModelsTrained-folder under
neural_network_tuned.data. Pickle is used to serialize the model,
before gzip is used to store it.

To see how the model performs, run the neural_network_tuned_test.py script.

This script requires that `pandas`, `sklearn`, `gzip` and `pickle` are
installed within the Python environment you are running this script in.
"""

import gzip
import pickle
import pandas as pd
from sklearn.neural_network import MLPClassifier
from Source.constants import DROP_COLUMNS, PREDICT_COLUMN
from Source.train_test_split import train_test_split

# Import preprocessed data
df = pd.read_csv("../Data/preprocessed_student_mat.csv")

# Split data into test and train data
X_train, X_test, y_train, y_test = train_test_split(df, DROP_COLUMNS, PREDICT_COLUMN, percentile=90)


# Create a neural network classifier with tuned hyperparameters
model = MLPClassifier(solver="sgd", alpha=0.001, hidden_layer_sizes=(250, 100, 250), shuffle=True,
                      max_iter=1000, activation='tanh', random_state=23, tol=0.001, learning_rate='constant')


# Fit it to the training data
model.fit(X_train, y_train)

# Store the trained model
with gzip.open("../ModelsTrained/neural_network_tuned.data", "w") as f:
    pickle.dump(model, f)





