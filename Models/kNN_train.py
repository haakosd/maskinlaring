"""kNN Train

This script allows the user to train a kNN-classifier on the Student Alcohol
Consumption data set found on
https://www.kaggle.com/uciml/student-alcohol-consumption.

The trained model is stored in the /ModelsTrained-folder under knn.data.
Pickle is used to serialize the model, before gzip is used to store it.

To see how the model performs, run the kNN_test.py-script.

This script requires that `pandas`, `sklearn`, `gzip` and `pickle` are installed
within the Python environment you are running this script in:
"""

import gzip
import pickle
import pandas as pd
from sklearn.neighbors import KNeighborsClassifier
from Source.constants import DROP_COLUMNS, PREDICT_COLUMN
from Source.train_test_split import train_test_split

# Import preprocessed data
df = pd.read_csv("../Data/preprocessed_student_mat.csv")

# Split data into test and train data
X_train, X_test, y_train, y_test = train_test_split(df, DROP_COLUMNS, PREDICT_COLUMN, percentile=90)

# Create a 5-NN classifier with default parameters
model = KNeighborsClassifier(n_neighbors=5)

# Fit it to the training data
model.fit(X_train, y_train)

# Store the trained model
with gzip.open("../ModelsTrained/knn.data", "w") as f:
    pickle.dump(model, f)
