"""Neural Network Tuned Test

This script allows the user to test the tuned neural network trained in
neural_network_tuned_train.py.

The trained model is stored in the /ModelsTrained-folder under
neural_network_tuned.data. Pickle is used to serialize the model,
before gzip is used to store it.

To see how the model performs, simply run the script from top to bottom.
The script assumes the trained model is already stored in the
/ModelsTrained-folder. The tuned model is the model with the best parameters
found in the randomized search.

This script requires that `pandas`, `sklearn`, `gzip` and `pickle` are installed
within the Python environment you are running this script in:
"""

import gzip
import pickle
import pandas as pd
import sklearn
from Source.constants import DROP_COLUMNS, PREDICT_COLUMN
from Source.train_test_split import train_test_split

# Import preprocessed data
df = pd.read_csv("../Data/preprocessed_student_mat.csv")

# Split data into test and train data
X_train, X_test, y_train, y_test = train_test_split(df, DROP_COLUMNS, PREDICT_COLUMN, percentile=90)

# Load the trained model
with gzip.open("../ModelsTrained/neural_network_tuned.data") as f:
    model = pickle.load(f)

# Make in- and out-of-sample predictions
y_train_hat = model.predict(X_train)
y_test_hat = model.predict(X_test)

# Print out classification results
r_train = sklearn.metrics.classification_report(y_train, y_train_hat, zero_division=0)
print('{0:->53}\n{0:>25}Train{0:<22}\n{0:->53}'.format(""))
print(r_train)
r_test = sklearn.metrics.classification_report(y_test, y_test_hat, zero_division=0)
print('{0:->53}\n{0:>25}Test {0:<22}\n{0:->53}'.format(""))
print(r_test)





