"""Random ForestTrain

This script allows the user to train a random forest with the on the Student
Alcohol Consumption data set found on Kaggle:
https://www.kaggle.com/uciml/student-alcohol-consumption.

The trained model is stored in the /ModelsTrained-folder under
random_forest.data. Pickle is used to serialize the model, before gzip is
used to store it.

To see how the model performs, run the random_forest_test.py script.

This script requires that `pandas`, `sklearn`, `gzip` and `pickle` are
installed within the Python environment you are running this script in.
"""

import gzip
import pickle
import pandas as pd
import sklearn.ensemble
from Source.constants import DROP_COLUMNS, PREDICT_COLUMN
from Source.train_test_split import train_test_split

# Import preprocessed data
df = pd.read_csv("../Data/preprocessed_student_mat.csv")

# Split data into test and train data
X_train, X_test, y_train, y_test = train_test_split(df, DROP_COLUMNS, PREDICT_COLUMN, percentile=60)

# Create a random forest classifier with default parameters
model = sklearn.ensemble.RandomForestClassifier(random_state=62)

# Fit it to the training data
model.fit(X_train, y_train)

# Load the trained model
with gzip.open("../ModelsTrained/random_forest.data", "w") as f:
    pickle.dump(model, f)
