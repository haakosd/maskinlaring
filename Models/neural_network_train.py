"""Neural Network Train

This script allows the user to train a multi-layer perceptron on the Student
Alcohol Consumption data set found on
https://www.kaggle.com/uciml/student-alcohol-consumption.

The trained model is stored in the /ModelsTrained-folder under neural_network.data.
Pickle is used to serialize the model, before gzip is used to store it.

To see how the model performs, run the neural_network_test.py script.

This script requires that `pandas`, `sklearn`, `gzip` and `pickle` are installed
within the Python environment you are running this script in.
"""

import gzip
import pickle
import pandas as pd
from sklearn.neural_network import MLPClassifier
from Source.constants import DROP_COLUMNS, PREDICT_COLUMN
from Source.train_test_split import train_test_split

# Import preprocessed data
df = pd.read_csv("../Data/preprocessed_student_mat.csv")

# Split data into test and train data
X_train, X_test, y_train, y_test = train_test_split(df, DROP_COLUMNS, PREDICT_COLUMN, percentile=10)

# Create a multi-layer perceptron classifier with three hidden layers of size 8
model = MLPClassifier(solver="lbfgs", alpha=1e-3, hidden_layer_sizes=(8, 8, 8),
                      max_iter=4000, activation='relu', random_state=123)

# Fit it to the training data
model.fit(X_train, y_train)

# Store the trained model
with gzip.open("../ModelsTrained/neural_network.data", "w") as f:
    pickle.dump(model, f)





