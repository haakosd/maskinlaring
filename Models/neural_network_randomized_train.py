"""Neural Network Randomized Train

This script allows the user to perform a randomized search on optimal
hyperparameters used in the training of a neural network used on the
Student Alcohol Consumption data set found on Kaggle:
https://www.kaggle.com/uciml/student-alcohol-consumption.

The trained model is stored in the /ModelsTrained-folder under
neural_network_randomized.data. Pickle is used to serialize the model,
before gzip is used to store it.

To see how the model performs, run the neural_network_randomized_test.py
script. The optimal parameters are stored under
/ModelsTrained/nn_rnd_params.json.

This script requires that `pandas`, `sklearn`, `itertools`, `gzip`, `pickle`,
`json` and `numpy` are installed within the Python environment you are running
this script in.
"""

import gzip
import itertools
import pickle
import json
import pandas as pd
import numpy as np
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import RandomizedSearchCV

from Source.constants import DROP_COLUMNS, PREDICT_COLUMN
from Source.train_test_split import train_test_split

# Import preprocessed data
df = pd.read_csv("../Data/preprocessed_student_mat.csv")

# Create a random forest classifier with default parameters
model = MLPClassifier(random_state=123, max_iter=1000)

# Define the ranges of the hyperparameter values

# Determine the hidden layer sizes. This creates tuples with all combinations
# of the numbers in `sizes`
sizes = (10, 50, 100, 250)
hidden_layer_sizes = [x for x in itertools.product(sizes, repeat=3)]
hidden_layer_sizes.extend([x for x in itertools.product(sizes, repeat=2)])
hidden_layer_sizes.extend([x for x in itertools.product(sizes, repeat=1)])

# Set the different activation functions
activation = ['identity', 'logistic', 'tanh', 'relu']
# Set solvers
solver = ['lbfgs', 'sgd', 'adam']
# Set the alpha values from 1e-07 to 0.1
alpha = [x for x in 10.**np.arange(-7, 0)]
# Set the learning rates
learning_rate = ['constant', 'invscaling', 'adaptive']
# Whether to shuffle samples in each iteration or not
shuffle = [True, False]
# Set the tolerance values from 1e-07 to 0.1
tol = [x for x in 10.**np.arange(-7, 0)]

# Define the random grid
random_grid = {'hidden_layer_sizes': hidden_layer_sizes,
               'activation': activation,
               'solver': solver,
               'alpha': alpha,
               'learning_rate': learning_rate,
               'shuffle': shuffle,
               'tol': tol
               }

# Initiate random search with cross validation
rf_model = RandomizedSearchCV(model, random_grid, n_iter=100, n_jobs=-1, cv=3)

# Iterate on percentile of the optimal attributes in the data set and store the best models
best_score = 0
best_model = rf_model
best_percentile = 0
models = dict()
for percentile in range(10, 110, 10):
    # Split data into test and train data
    X_train, X_test, y_train, y_test = train_test_split(df, DROP_COLUMNS, PREDICT_COLUMN, percentile=percentile)
    rf_model.fit(X_train, y_train)
    test_score = rf_model.score(X_test, y_test)
    print(test_score)
    models[percentile] = [rf_model.best_score_, test_score, rf_model.best_params_]
    if rf_model.best_score_ > best_score:
        best_score = rf_model.best_score_
        best_model = rf_model

# Store the trained model
with gzip.open("../ModelsTrained/neural_network_randomized.data", "w") as f:
    pickle.dump(best_model, f)

# Store all best params with corresponding score
with open("../ModelsTrained/nn_rnd_params.json", "w") as f:
    json.dump(models, f)
