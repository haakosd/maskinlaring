"""Preprocess

This script allows the user to preprocess the raw data downloaded from
https://www.kaggle.com/uciml/student-alcohol-consumption.

The preprocessing script label-encodes all the categorical data as well as
scaling the grades from 1-20 down to 1-5.

This script requires that `pandas` and `sklearn` are
installed within the Python environment you are running this script in.
"""

import pandas as pd
from sklearn import preprocessing

# Read the raw data
df = pd.read_csv("../Data/raw_student_mat.csv")

# Convert categorical columns to integer values
# Categorical boolean mask
categorical_feature_mask = df.dtypes == object
# Filter categorical columns using mask and turn it into a list
categorical_cols = df.columns[categorical_feature_mask].tolist()
le2 = preprocessing.LabelEncoder()
# Apply label encoder on categorical feature columns
df[categorical_cols] = df[categorical_cols].apply(lambda col: le2.fit_transform(col))

# Create predict column from the average grade of the students scaled from 1 to 5
df['Grade'] = round((df['G1'] + df['G2'] + df['G3'])/3/5) + 1
df['Grade'] = df['Grade'].astype(int)

# Save the preprocessed data
df.to_csv("../Data/preprocessed_student_mat.csv", index=False)
