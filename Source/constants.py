"""Constants

This file stores constants used when filtering the data set for consistency in
all models.
"""

DROP_COLUMNS = ['school', 'guardian', 'G1', 'G2', 'G3', 'Grade']
PREDICT_COLUMN = 'Grade'
