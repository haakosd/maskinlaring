"""Train Test Split

This script allows the user to split a Pandas Data Frame into train and test
data frames for training and testing a model respectively.

This script requires that `sklearn` is installed within the Python environment
you are running this script in.
"""

import sklearn
from sklearn.feature_selection import SelectPercentile, chi2


def train_test_split(data_frame, drop_columns, predict_column, percentile=100):
    # Separate features from targets
    X = data_frame.drop(columns=drop_columns)
    y = data_frame[predict_column]

    X_sp = SelectPercentile(chi2, percentile=percentile).fit_transform(X, y)


    X_train, X_test, y_train, y_test = sklearn.model_selection.train_test_split(
        # Data to split up
        X_sp, y,
        # Fraction of samples in the train set (80%)
        train_size=0.8,
        # Shuffle before splitting, so that the sets are sampled randomly
        shuffle=True,
        # Random seed, for reproducibility
        random_state=123
    )

    return X_train, X_test, y_train, y_test
