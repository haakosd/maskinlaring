"""Random Forest Visualizer

This script allows the user to visualize a tree in a random forest.

This script requires that `pandas`, `sklearn`, `subprocess` and `IPython`
are installed within the Python environment you are running this script in.
"""

import gzip
import pickle
import pandas as pd
import sklearn.ensemble
from sklearn.tree import export_graphviz
from subprocess import call
# Display in jupyter notebook
from IPython.display import Image

from Source.constants import DROP_COLUMNS, PREDICT_COLUMN
from Source.train_test_split import train_test_split

# Import preprocessed data
df = pd.read_csv("../Data/preprocessed_student_mat.csv")

# Split data into test and train data
X_train, X_test, y_train, y_test = train_test_split(df, DROP_COLUMNS, PREDICT_COLUMN, percentile=100)

# Create a random forest classifier with default parameters
model = sklearn.ensemble.RandomForestClassifier(random_state=62, n_estimators=10, max_depth=3, max_leaf_nodes=5)

# Fit it to the training data
model.fit(X_train, y_train)

# Load the trained model
with gzip.open("../ModelsTrained/random_forest.data", "w") as f:
    pickle.dump(model, f)

# Extract single tree
estimator = model.estimators_[5]
# Export as dot file
export_graphviz(estimator, out_file='tree.dot',
                feature_names = df.drop(columns=DROP_COLUMNS).columns,
                class_names=["1", "2", "3", "4", "5"],
                rounded=True, proportion=False,
                precision=2, filled=True)
call(['dot', '-Tpng', 'tree.dot', '-o', 'tree2.png', '-Gdpi=600'])

# Convert to png using system command (requires Graphviz)
Image(filename='tree2.png')

