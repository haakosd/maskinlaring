"""Heatmap

This script allows the user to plot a heatmap on the correlations between
the attributes in the Student Alcohol Consumption data set found on Kaggle:
https://www.kaggle.com/uciml/student-alcohol-consumption.

This script requires that `pandas`, `seaborn` and `matplotlib` are
installed within the Python environment you are running this script in.
"""

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt


# Import preprocessed data
df = pd.read_csv("../Data/preprocessed_student_mat.csv")

# Preselect the set of columns we want to visualize
df = df[['sex', 'age', 'Medu', 'Fedu', 'Mjob', 'Fjob', 'traveltime', 'goout', 'studytime', 'Dalc', 'Walc', 'G1', 'G2', 'G3']]

# Use seaborn to plot the heatmap
plt.figure(figsize=(16, 16))
plt.title("Column Correlation")
plt.xticks(rotation=90)
sns.heatmap(df.corr(), annot=True, fmt='.2f', cbar=True, square=True)
plt.show()
