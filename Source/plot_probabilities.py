"""Plot Probabilities

This script allows the user to plot the likelihood of a student getting
a particular grade based on the result from a neural network.

The trained model is stored in the /ModelsTrained-folder under
neural_network_tuned.data. Pickle is used to serialize the model,
before gzip is used to store it.

This script requires that `pandas`, `sklearn`, `seaborn`, `matplotlib`, `gzip`
and `pickle` are installed within the Python environment you are running this script in
"""

import gzip
import pickle
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from Source.constants import DROP_COLUMNS, PREDICT_COLUMN
from Source.train_test_split import train_test_split

# Import preprocessed data
df = pd.read_csv("../Data/preprocessed_student_mat.csv")

# Split data into test and train data
X_train, X_test, y_train, y_test = train_test_split(df, DROP_COLUMNS, PREDICT_COLUMN, percentile=90)

# Load the trained model
with gzip.open("../ModelsTrained/neural_network_tuned.data") as f:
    model = pickle.load(f)

# Get the probabilities on the test data
probs = model.predict_proba(X_test)
pred = model.predict(X_test)

# Print the actual values for two of the instances
print(y_test[y_test.index[38]])
print(y_test[y_test.index[5]])

# Plot the softmax output from the network for tw instances
x = [1, 2, 3, 4, 5]
freq = probs[38]
freq2 = probs[5]
df = pd.DataFrame({'Grade': x, 'Prediction': freq})
df2 = pd.DataFrame({'Grade': x, 'Prediction': freq2})
fig, ax = plt.subplots(1, 2, figsize=(8, 4))
sns.barplot(data=df, x='Grade', y='Prediction', palette="rocket", ax=ax[0])
sns.barplot(data=df2, x='Grade', y='Prediction', palette="rocket", ax=ax[1])
fig.show()


