"""Class Distribution

This script allows the user to visualize the class distribution of the
grades of the students in the Student Alcohol Consumption data set found
on Kaggle:
https://www.kaggle.com/uciml/student-alcohol-consumption.

This script requires that `pandas`, `seaborn` and `matplotlib` are
installed within the Python environment you are running this script in.
"""

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt


# Import preprocessed data
df = pd.read_csv("../Data/preprocessed_student_mat.csv")

# Use seaborn to plot class distribution
sns.displot(df, x="Grade", discrete=True, shrink=.8)
plt.show()
