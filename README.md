# Machine Learning Models for Predicting Student Grades

This repository contains the code used in the course TDT4173 Machine Learning at NTNU.

The project aims to predict student performance based on different data about each student.

## Getting Started

In order to run the code, you will need an installation of Python. The different project dependencies are listed in `requirements.txt`.

A Python virtualenv is recommended when installing the dependencies.

1. Clone the repo
2. Install the different dependencies from `requirements.txt`
3. Run any of the models in the `Models`-folder

## Code Overview

The different folders of the project are descibed below.

    .
    ├── Data                          # The raw and preprocessed data
    ├── Models                        # The different models, both code for training and testing
    ├── ModelsTrained                 # Zip-files of already trained models used for testing
    ├── Source                        # Other code for visualizations, preprocessing, etc.
    ├── .gitignore
    ├── README.md
    └── requirements.txt              # List of required packages for the environment the scripts are run in
    
More specific information on the files in each folder is found on the top of each script.
    
## The Data Set

The data set is called *Student Alcohol Consumption* and is downloaded from [Kaggle](https://www.kaggle.com/uciml/student-alcohol-consumption).

For this project, only the `student-mat.csv` data set is used.
