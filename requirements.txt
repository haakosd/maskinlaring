pandas~=1.1.4
numpy~=1.19.4
sklearn~=0.0
scikit-learn~=0.23.2
seaborn~=0.11.0
matplotlib~=3.3.2

# Only for visualizing trees in random forest
ipython~=7.19.0